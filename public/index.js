"use strict";

const main = (()=> {
    const emailInput = document.querySelector('#email-input');
    emailInput.addEventListener('input', (e) => {
        if (emailInput.validity.typeMismatch) {
            emailInput.setCustomValidity("I am expecting an e-mail address!");
        } else {
            emailInput.setCustomValidity("");
        }
    });
    
    const postcodeInput = document.querySelector('#postcode-input');
    postcodeInput.addEventListener('input', (e) => {
        if (postcodeInput.validity.patternMismatch) {
            postcodeInput.setCustomValidity("Please enter a valid UK postcode");
        } else {
            postcodeInput.setCustomValidity("");
        }
    });

    const passwordInput = document.querySelector('#password-input');
    const passwordInputValidation = document.querySelector('#password-input-validation');
    passwordInputValidation.addEventListener('input', (e) => {
        if (passwordInput.value !== passwordInputValidation.value) {
            passwordInputValidation.setCustomValidity("Passwords do not match.");
        } else {
            passwordInputValidation.setCustomValidity("");
        }
    });
})();

